#ifndef INC_PROGRAM_H
#define INC_PROGRAM_H

#include <stdint.h>
#include "element.h"
#include "hash.h"

/* Only inline functions if not building debug; ie with -finline-functions. */
#ifdef NDEBUG
    #define FINLINE inline
#else
    #define FINLINE
#endif

/* Available instruction types in the bytecode */
typedef enum {
    JUMP,
    REACT
} OpCode;

/* Available reaction processes */
typedef enum {
    CALCINATE, DIGEST, FERMENT, COAGULATE, FIXATE, CERATE,
    DISTILL, SUBLIMATE, FILTER, FUSE, MULTIPLY, PROJECT,
    PROCESS_COUNT
} ReactionProcess;

/* Possible outcome errors that result in termination */
typedef enum {
    OK,
    /* Runtime explosions */
    EXPL_INSUFFICIENT_ALKAHEST, EXPL_INSOLUBILITY, EXPL_EXCESSIVE_SOLVENT,
    EXPL_INSUFFICIENT_SOLVENT, EXPL_INTANGIBLE_ELEMENT,
    EXPL_UNDEFINED_STEP, EXPL_UNDEFINED_INSTRUCTION,
    /* Compile-time explosions */
    EXPL_AMBIGUOUS_STEP, EXPL_ERRONEOUS_UNITS, EXPL_ALTERING_REALITY,
    EXPL_IRRATIONAL_REAGENT, EXPL_REAGENT_SHORTAGE, EXPL_MALFORMED_ELEMENT,
    EXPL_UNDEFINED_REACTION, EXPL_SYNTAX_ERROR
} Result;

typedef struct {
    uint16_t number;
    uint32_t offset;
} Step;

/* An instruction; either for jumping or for reacting */
typedef struct {
    OpCode opcode;
    union {
        struct {
            Element *element;
            Step *step;
        } j;
        struct {
            ReactionProcess process;
            Reagents reagents;
            Element *product;
        } r;
    } data;
} Instruction;

/* An Alchemy program with its compiled bytecode and state */
typedef struct {
    uint16_t *code, code_size;
    Element *elements;
    Step *steps;
    uint32_t counter, line_num, num_elements, num_steps;
    char has_alkahest;
} Program;


Program*    program_create      (FILE *file, Result *result);
void        program_destroy     (Program *program);
Result      program_execute     (Program *program);
Element*    program_get_element (Program *program, uint16_t index);

const char* result_string   (Result result);

#endif

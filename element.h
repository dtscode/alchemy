#ifndef INC_ELEMENT_H
#define INC_ELEMENT_H

#include <stdint.h>

/* Constants */
#define NAME_MAX                30
#define MAX_REAGENTS            20
#define ALKAHEST_AMOUNT         813
#define BUILTIN_COUNT           9
#define ATTR_CONSTANT           1
#define ATTR_PRODUCT            2

/* A named value with special properties */
typedef struct {
    char name[NAME_MAX + 1];
    uint32_t subnum;
    char dry, aqueous, initialized, attributes;
    double value;
} Element;

/* All possible solvent chemicals */
typedef enum {
    ALKAHEST,
    WATER
} Solvent;

/* A list of elements, and a solvent, for use in a reaction */
typedef struct {
    Element *elements[MAX_REAGENTS];
    uint32_t amounts[MAX_REAGENTS];
    Solvent solvent;
    uint32_t solvent_amount, count;
} Reagents;

/* The list of properties representing builtin elements */
typedef struct {
    char *name;
    double value;
    uint8_t attributes;
} BuiltinElement;

BuiltinElement builtin_elements[BUILTIN_COUNT];


Element*    element_create          (const char *name);
int         element_init            (Element *element, const char *name);
void        element_destroy         (Element *element);
void        element_set_value       (Element *element, double value);

Reagents*   reagents_create         ();
void        reagents_destroy        (Reagents *reagents);
void        reagents_add_element    (Reagents *reagents, Element *element,
                                     uint32_t amount);
void        reagents_clear          (Reagents *reagents);
void        reagents_set_solvent    (Reagents *reagents, Solvent solvent,
                                     uint32_t amount);
uint32_t    reagents_needed_solvent (Reagents *reagents);
int         reagents_are_soluble    (Reagents *reagents);

#endif
